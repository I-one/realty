<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
?>
<style type="text/css">
	.form-label {}
	.form-label label {
		float: left;
		width: 100%;
	}
</style>
<?php if ( is_front_page() && is_home() ) : ?>
	<?php get_template_part( 'global-templates/hero' ); ?>
<?php endif; ?>

<div class="wrapper" id="index-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">
		<h1 align="center">Недвижимость</h1>
		<div class="row">
			<!-- Do the left sidebar check and opens the primary div -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>
			<?$args = array(
				'post_type' => 'post',
				'posts_per_page' =>	'6',
				'category_name' => 'realty'
				);
			?>
			<?$wp_query = new WP_Query($args);?>
			<?while ($wp_query->have_posts()):
				$wp_query->the_post();?>
				<?php

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'loop-templates/content', get_post_format() );
				?>
			<?php endwhile; ?>
			<!-- Do the right sidebar check -->
			<?php get_template_part( 'global-templates/right-sidebar-check' ); ?>
		</div><!-- .row -->
	</div><!-- Container end -->
	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">
		<h1 align="center">Города</h1>
		<div class="row">
			<!-- Do the left sidebar check and opens the primary div -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>
			<?
			$idObj = get_category_by_slug('city');
			$child = $idObj->cat_ID;
			$args = array(
				'type'         => 'post',
				'child_of'     => $child,
				'hide_empty'   => 0,
			);
			$categories = get_categories( $args );
			foreach( $categories as $cat ){?>
				<div class="col-sm-4 content-area" id="primary">
					<header class="entry-header">
						<h2 class="entry-title"><a href="<?echo $cat_link = get_category_link( $cat->cat_ID );?>"><?echo $cat->name?></a></h2>
					</header><!-- .entry-header -->
					<?php if($imgcat1=get_field("imgcat1",$cat)){
						$image = aq_resize($imgcat1, 285, 200, true );?>
						<div class="img">
							<img src="<?php echo $image;?>"/>
						</div>
					<?php }?>
					<div class="entry-content" style="width:250px;">
						<?php
						echo $cat->description;
						?>
					</div><!-- .entry-content -->
				</div><!-- #post-## -->
			<?}?>
			<!-- Do the right sidebar check -->
			<?php get_template_part( 'global-templates/right-sidebar-check' ); ?>
		</div><!-- .row -->
	</div><!-- Container end -->
	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">
		<h1 align="center">Добавление нового объекта недвижимости</h1>
		<div class="row">
			<?php
			// подготовим актуальные данные таксономий
			$cats = get_terms(array('taxonomy' => 'category','hide_empty' => true, 'parent' => 0)); // получим все термины(элементы) таксономии с иерархией
			foreach ($cats as $cat) { // пробежим по каждому полученному термину
				$parents.="<option value='$cat->term_id' />$cat->name</option>"; // суем id и название термина в строку для вывода внутри тэга select
				$childs_array = get_terms(array('taxonomy' => 'category', 'hide_empty' => 0, 'orderby' => 'name', 'parent' => $cat->term_id)); // возьмем все дочерние термины к текущему'
				foreach ($childs_array as $child){
					$childs.="<option value='$child->term_id' class='$cat->term_id' />$child->name</option>"; // делаем то же самое, класс должен быть равным id родительского термина чтобы плагин chained работал
				}
			}
			?>
			<?php // Выводим форму ?>
			<form method="post" enctype="multipart/form-data" id="add_object">
				<div class="form-label">
					<label>Родительская категория:
						<select id="parent_cats" name="parent_cats" required>
							<option value="">Не выбрано</option>
							<?php echo $parents; // выводим все родительские термины ?>
						</select>
					</label>
					<label>Дочерняя категория:
						<select id="child_cats" name="child_cats" required>
							<option value="">Не выбрано</option>
							<?php echo $childs; // выводим все дочерние термины, плагин chained сам покажет только нужные элементы в зависимости от выбранного родительского термина ?>
						</select>
					</label>
					<label>Заголовок <input type="text" name="post_title" required/></label>
					<label>Контент <textarea name="post_content" required/></textarea></label>
					<label>Площадь <input type="text" name="square"/></label>
					<label>Стоимость <input type="number" name="price"/></label>
					<label>Адрес <input type="text" name="address"/></label>
					<label>Жилая площадь <input type="number" name="kv_m"/></label>
					<label>Этаж <input type="number" name="floor"/></label>
					<label>Миниатюра: <input type="file" name="img"/></label>
					<input type="submit" name="button" value="Отправить" id="sub"/>
					<div id="output"></div> <?php // сюда будем выводить ответ */?>
				</div>
			</form>
		</div><!-- .row -->
	</div><!-- Container end -->
</div><!-- Wrapper end -->

<?php get_footer(); ?>
