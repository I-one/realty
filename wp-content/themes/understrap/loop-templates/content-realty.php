<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<div class="col-sm-8 content-area" id="primary">

	<header class="entry-header">

		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

	</header><!-- .entry-header -->

	<?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?>

	<div class="entry-content">

		<?php the_content(); ?>
		<?php
		wp_link_pages( array(
			'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
			'after'  => '</div>',
		) );
		?>

	</div><!-- .entry-content -->
</div>
<div class="col-sm-4 content-area" id="primary">
	<div class="entry-content" style="padding-top:50px;">
		<b>Общая площадь составляет:</b> <?=get_post_meta($post->ID, 'square', true)?> кв/м.</br>
		<b>Стоимость:</b> <?=get_post_meta($post->ID, 'price', true);?> руб.</br>
		<b>Адрес:</b> <?=get_post_meta($post->ID, 'adress', true);?></br>
		<b>Жилая площадь:</b> <?=get_post_meta($post->ID, 'kv_m', true);?> кв/м.</br>
		<b>Этаж:</b> <?=get_post_meta($post->ID, 'floor', true);?></br>
	</div><!-- .entry-content -->
</div>
