<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<div class="col-sm-4 content-area">
	<header class="entry-header">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
		'</a></h2>' ); ?>
	</header><!-- .entry-header -->
	<?php
	$thumb = get_post_thumbnail_id();
	$img_url = wp_get_attachment_url( $thumb,'full' ); //get full URL to image (use "large" or "medium" if the images too big)
	$image = aq_resize( $img_url, 285, 200, true ); //resize & crop the image
	?>
	<img src="<?php echo $image ?>"/>
	<div class="entry-content">
		<b>Общая площадь составляет:</b> <?=get_post_meta($post->ID, 'square', true)?> кв/м.</br>
		<b>Стоимость:</b> <?=get_post_meta($post->ID, 'price', true);?> руб.</br>
		<b>Адрес:</b> <?=get_post_meta($post->ID, 'adress', true);?></br>
		<b>Жилая площадь:</b> <?=get_post_meta($post->ID, 'kv_m', true);?> кв/м.</br>
		<b>Этаж:</b> <?=get_post_meta($post->ID, 'floor', true);?></br>
		<?php
		the_excerpt();
		?>
		<?php
		wp_link_pages( array(
			'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
			'after'  => '</div>',
		) );
		?>

	</div><!-- .entry-content -->
</div><!-- #post-## -->