<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<div class="col-sm-4 content-area" id="primary">
	<header class="entry-header">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', $cat->name ),
		'</a></h2>' ); ?>
	</header><!-- .entry-header -->
	<?php
	$thumb = get_post_thumbnail_id();
	$img_url = wp_get_attachment_url( $thumb,'full' ); //get full URL to image (use "large" or "medium" if the images too big)
	$image = aq_resize( $img_url, 285, 200, true ); //resize & crop the image
	?>
	<img src="<?php echo $image ?>"/>
	<div class="entry-content">
		<?php
		the_excerpt();
		?>
	</div><!-- .entry-content -->
</div><!-- #post-## -->